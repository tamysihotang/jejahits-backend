# README #

project-backend is API for backend project

* Repo backend untuk project 
* Versi 1.0.0

### PREREQ ###

* Install Java 1.8
* Install Maven 3
* Install MySQL
* Install Java Lombok Plugin on IDE

### HOW TO START ###
* create database : academy , username : academy , password : welcome1. (based on jdbc.properties)
* cd nostratech-backend
* mvn clean package
* mvn spring-boot:run
#!/bin/bash
scp -i id_rsa jejahits-backend/target/jejahits_backend-1.0.0.RELEASE.jar nostra@34.87.127.32:
ssh -i id_rsa nostra@34.87.127.32 "cd /opt/project && sudo ./projectctl stop"
ssh -i id_rsa nostra@34.87.127.32 "cd /opt/project && sudo rm -rf jejahits_backend-1.0.0.RELEASE.jar"
ssh -i id_rsa nostra@34.87.127.32 "sudo mv jejahits_backend-1.0.0.RELEASE.jar /opt/project"
ssh -i id_rsa nostra@34.87.127.32 "cd /opt/project && sudo ./projectctl start"

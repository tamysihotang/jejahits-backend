package com.jejahits.project.service;

import com.jejahits.project.converter.ProductVOConverter;
import com.jejahits.project.converter.VendorVOConverter;
import com.jejahits.project.enums.Direction;
import com.jejahits.project.enums.ServiceType;
import com.jejahits.project.enums.SortBy;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.util.Constants;
import com.jejahits.project.util.DocumentUtils;
import com.jejahits.project.validator.ProductValidator;
import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import com.jejahits.project.vo.ResponseVO.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductVOConverter productVOConverter;

    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    ProductValidator productValidator;

    @Autowired
    DocumentUtils documentUtils;

    @Autowired
    KategoryRepository kategoryRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KecamatanRepository kecamatanRepository;

    @Autowired
    VendorVOConverter vendorVOConverter;

    public Boolean createProduct(List<ProductRequestVO> productRequestVOList, String vendorId){

        Vendor vendor = vendorRepository.findBySecureId(vendorId);
        if(vendor==null){
            throw new JejahitsException("Vendor not found");
        }

        Product product = new Product();
        for(ProductRequestVO productRequestVO : productRequestVOList){
            Kategory kategory = kategoryRepository.findBySecureId(productRequestVO.getKategoryId());
            if(null==kategory){
                throw new JejahitsException("Kategory not Found");
            }
            List<Product> productExists = productRepository.findByKategoryAndServiceTypeAndVendor(kategory, productRequestVO.getServiceType(),vendor);
            if(productExists.isEmpty()){
                String messageValidation = productValidator.validationCreateProduct(productRequestVO);
                if (null != messageValidation) throw new JejahitsException(messageValidation);

                productVOConverter.transferVOToModel(productRequestVO,product);
                product.setVendor(vendor);
                if(productRequestVO.getImage().size()>0){
                    for(int i = 0; i<productRequestVO.getImage().size();i++){
                        if(i==0){
                            product.setFile1(productRequestVO.getImage().get(0).getFileUrl());
                        }
                        if(i==1){
                            product.setFile2(productRequestVO.getImage().get(1).getFileUrl());
                        }
                        if(i==2){
                            product.setFile3(productRequestVO.getImage().get(2).getFileUrl());
                        }
                    }
                }
                productRepository.save(product);
            }else{
                throw new JejahitsException("Product has already registered");
            }


        }

        return Boolean.TRUE;
    }

    public Map<String, Object> listProduct(Integer page, Integer limit, String vendorId, String kategoryId){

//        vendorId = vendorId.equals("%") ? "%" : "%" + vendorId + "%";
//        kategoryId = kategoryId.equals("%") ? "%" : "%" + kategoryId + "%";
        String sortBy = "Id";
        String direction = "desc";
        log.info("kategory "+kategoryId);
        log.info("vendor "+vendorId);


        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        Page<Product> resultPage =null;
        Vendor vendor = vendorRepository.findBySecureId(vendorId);
        Kategory kategory = kategoryRepository.findBySecureId(kategoryId);

        if(!vendorId.equals("%") && kategoryId.equals("%")){
            log.info("1 ");
            resultPage = productRepository.findByVendor(vendor, pageable);
        }else if(!kategoryId.equals("%") && vendorId.equals("%")){
            log.info("2 ");
            resultPage = productRepository.findByKategory(kategory,pageable);
        }else if(!vendorId.equals("%") && !kategoryId.equals("%")){
            log.info("3 ");
            resultPage = productRepository.findByVendorAndKategory(vendor, kategory,pageable);
        }else{
            log.info("4 ");
            resultPage = productRepository.findAll(pageable);
        }
        log.info("resultPage "+resultPage);
        List<Product> models = resultPage.getContent();

        Collection<ProductVO> vos = productVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    public Map<String, Object> mylistProduct(Integer page, Integer limit, String vendorId){

        String sortBy = "Id";
        String direction = "desc";


        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        Page<Product> resultPage =null;
//        User user = userRepository.findByUsername(username);
        Vendor vendor = vendorRepository.findBySecureId(vendorId);
        if(null==vendor){
            throw new JejahitsException("Vendor not Found");
        }

        resultPage = productRepository.findByVendor(vendor, pageable);

        log.info("resultPage "+resultPage);
        List<Product> models = resultPage.getContent();
        Collection<ProductVendorVO> productVendorVOArrayList= new ArrayList<>();
        for(Product product : resultPage){
            ProductVendorVO productVendorVO = new ProductVendorVO();
            productVendorVO.setKategory(product.getKategory().getKategoryName());
            productVendorVO.setHargaRangeAwal(product.getHargaRangeAwal());
            productVendorVO.setHargaRangeAkhir(product.getHargaRangeAkhir());
            productVendorVO.setServiceType(product.getServiceType());
            productVendorVO.setRating(product.getRating());
            List<FileUploadVO> fileUploadVOS =new ArrayList<>();
            if(product.getFile1()!=null){
                FileUploadVO fileUploadVO = new FileUploadVO();
                fileUploadVO.setFileName(product.getFile1());
                fileUploadVO.setFileUrl(product.getFile1());
                fileUploadVOS.add(fileUploadVO);
            }
            if(product.getFile2()!=null){
                FileUploadVO fileUploadVO = new FileUploadVO();
                fileUploadVO.setFileName(product.getFile2());
                fileUploadVO.setFileUrl(product.getFile2());
                fileUploadVOS.add(fileUploadVO);
            }
            if(product.getFile3()!=null){
                FileUploadVO fileUploadVO = new FileUploadVO();
                fileUploadVO.setFileName(product.getFile3());
                fileUploadVO.setFileUrl(product.getFile3());
                fileUploadVOS.add(fileUploadVO);
            }
            productVendorVO.setImage(fileUploadVOS);
            productVendorVOArrayList.add(productVendorVO);
        }
        Collection<ProductVendorVO> vos = productVendorVOArrayList;
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    @Transactional
    public FileUploadVO uploadFoto(MultipartFile multipartFile) {
        FileUploadVO fileUploadVO = new FileUploadVO();
        // get file
//        String messageValidation = productValidator.checkMultipartFile(multipartFile);
//        if (null != messageValidation) throw new JejahitsException(messageValidation);
        Date now = new Date();
        String todayFormatting = new SimpleDateFormat("ddMMyyyyHHmmss").format(now);
        // save file
        String destination = "IMAGE" + todayFormatting + ".JPG";
        String upload = documentUtils.uploadFile(multipartFile, destination);
        log.info("upload "+upload);

        fileUploadVO.setFileName(destination);
        fileUploadVO.setFileUrl(upload+destination);

        if (upload.equalsIgnoreCase("ERROR")) {
            throw new JejahitsException("Gagal mengupload foto");
        }

            return fileUploadVO;
        }

        public Map<String, Object> listVendorPerKategory (Integer page, Integer limit, SortBy sortBy, Direction direction, String kategoryId, String provinsiId, String kabupatenId, String kecamatanId, Integer rating, String vendorName){
            vendorName = vendorName.equals("%") ? "%" : "%" + vendorName + "%";
            String sortByString ="";
            if(sortBy.name().equals("RATING")){
                sortByString = "rating";
            }
            Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortByString, direction.name()));
            Kategory kategory = kategoryRepository.findBySecureId(kategoryId);
            if(null==kategory){
                throw new JejahitsException("Kategory not Found");
            }
            Page<Vendor> vendorPage = null;
            List<String> vendorList = productRepository.findByKategorynative(kategory);
            log.info("vendorLists "+vendorList);
            if(vendorList.isEmpty()){
                throw new JejahitsException("Vendor Not Found");
            }
            Provinsi provinsi = provinsiRepository.findBySecureId(provinsiId);
            Kabupaten kabupaten = kabupatenRepository.findBySecureId(kabupatenId);
            Kecamatan kecamatan = kecamatanRepository.findBySecureId(kecamatanId);
            if(provinsi!=null && kabupaten==null && kecamatan==null && rating == 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndVendorNameLikeIgnoreCase(vendorList,provinsi,vendorName,pageable);
            }else if(provinsi!=null && kabupaten==null && kecamatan==null && rating != 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndRatingAndVendorNameLikeIgnoreCase(vendorList,provinsi,rating,vendorName,pageable);
            }else if(provinsi!=null && kabupaten!=null && kecamatan==null && rating == 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndKabupatenAndVendorNameLikeIgnoreCase(vendorList,provinsi,kabupaten,vendorName,pageable);
            }else if(provinsi!=null && kabupaten!=null && kecamatan==null && rating != 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndKabupatenAndRatingAndVendorNameLikeIgnoreCase(vendorList,provinsi,kabupaten,rating,vendorName,pageable);
            }else if (provinsi!=null && kabupaten!=null && kecamatan!=null && rating == 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndKabupatenAndKecamatanAndVendorNameLikeIgnoreCase(vendorList,provinsi,kabupaten,kecamatan,vendorName,pageable);
            }else if (provinsi!=null && kabupaten!=null && kecamatan!=null && rating != 0){
                vendorPage = vendorRepository.findBySecureIdInAndProvinsiAndKabupatenAndKecamatanAndRatingAndVendorNameLikeIgnoreCase(vendorList,provinsi,kabupaten,kecamatan,rating,vendorName,pageable);
            }else if (provinsi==null && kabupaten==null && kecamatan==null && rating != 0){
                vendorPage = vendorRepository.findBySecureIdInAndRatingAndVendorNameLikeIgnoreCase(vendorList,rating,vendorName,pageable);
            }else{
                vendorPage = vendorRepository.findBySecureIdInAndVendorNameLikeIgnoreCase(vendorList,vendorName,pageable);
            }

//            vendorPage = vendorRepository.findBySecureIdIn(vendorList,pageable);
            List<VendorVO> vendorVOList = new ArrayList<>();
            for (Vendor vendor : vendorPage){
                VendorVO vendorVO = new VendorVO();
                vendorVOConverter.transferModelToVO(vendor,vendorVO);
                vendorVOList.add(vendorVO);
            }

            return AbstractBaseVoService.constructMapReturn(vendorVOList, vendorPage.getTotalElements(), vendorPage.getTotalPages());
        }

        public ServiceVO listProductVendor(String vendorId){
            ServiceVO serviceVO = new ServiceVO();
            Vendor vendor = vendorRepository.findBySecureId(vendorId);
            if(null==vendor){
                throw new JejahitsException("Vendor not Found");
            }

            List<Product> productJahit = productRepository.findByVendorAndServiceTypeLikeIgnoreCase(vendor, Constants.ServiceType.JAHIT);
            JahitVO jahitVO = new JahitVO();
            Collection<ProductVO> vosJahit = productVOConverter.transferListOfModelToListOfVO(productJahit, new ArrayList<>());
            jahitVO.setProductVO(vosJahit);
            serviceVO.setJahit(jahitVO);

            List<Product> productVermak = productRepository.findByVendorAndServiceTypeLikeIgnoreCase(vendor, Constants.ServiceType.VERMAK);
            VermakVO vermakVO = new VermakVO();
            Collection<ProductVO> vosVermak = productVOConverter.transferListOfModelToListOfVO(productVermak, new ArrayList<>());
            vermakVO.setProductVO(vosVermak);
            serviceVO.setVermak(vermakVO);
        return serviceVO;
        }

        public Map<String, Object> listProductExclude(Integer page, Integer limit, String vendorId, String kategoryId){
            String sortBy = "Id";
            String direction = "desc";
            Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
            Vendor vendor = vendorRepository.findBySecureId(vendorId);
            if(null==vendor){
                throw new JejahitsException("Vendor not Found");
            }
            Kategory kategory = kategoryRepository.findBySecureId(kategoryId);
            if(null==kategory){
                throw new JejahitsException("Kategory not Found");
            }
            List<Product> listProduct = productRepository.findByVendorAndKategory(vendor, kategory);
            List<String> listProductId = new ArrayList<>();
            for(Product product : listProduct){
                listProductId.add(product.getSecureId());
                log.info("IDPRODUCT "+product.getId());
            }
            Page<Product> resultPage =null;
            resultPage = productRepository.findBySecureIdNotInAndVendor(listProductId,vendor, pageable);
            List<Product> models = resultPage.getContent();

            Collection<ProductVO> vos = productVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());


            return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }
}




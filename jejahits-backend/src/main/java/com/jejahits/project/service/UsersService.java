package com.jejahits.project.service;

import com.jejahits.project.converter.IBaseVoConverter;
import com.jejahits.project.converter.UserVoConverter;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.vo.*;
import com.jejahits.project.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;


@Service
public class UsersService extends BaseVoService<User, UserVO, UserVO> {


    @Autowired
    SignInService signInService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PrivilegeService privilegeService;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Autowired
    UserVoConverter userVoConverter;


    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;



    @Override
    protected BaseRepository<User> getJpaRepository() {
        return null;
    }

    @Override
    protected JpaSpecificationExecutor<User> getSpecRepository() {
        return null;
    }

    @Override
    protected IBaseVoConverter<UserVO, UserVO, User> getVoConverter() {
        return null;
    }
}

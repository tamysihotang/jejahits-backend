package com.jejahits.project.service;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.util.StatusCode;
import com.jejahits.project.vo.ResultVO;
import com.jejahits.project.vo.SignInRequestVO;
import com.jejahits.project.vo.SignInResponseVO;
import com.jejahits.project.vo.SignOutRequestVO;
import com.jejahits.project.oauth.token.store.JejahitsTokenStore;
import com.jejahits.project.util.ApplicationContextProvider;
import com.jejahits.project.util.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * agus w
 */

@Service
@Transactional(readOnly = true)
public class SignInService {

    public static final Logger logger = LoggerFactory.getLogger(SignInService.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Autowired
    AccessTokenRepository accessTokenRepository;

    @Value("${client.id.admin}")
    String clientIdAdmin;

    @Value("${client.id.user}")
    String clientIdUser;

    @Value("${client.id.hq}")
    String clientIdHq;

    @Value("${client.id.md}")
    String clientIdmd;

    @Value("${client.id.smk}")
    String clientIdsmk;


    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    VendorRepository vendorRepository;

    private SignInResponseVO doLoginUser(String reqPassword, User user, String type) {

//        ResultVO resultVO = new ResultVO();

        if (passwordEncoder.matches(reqPassword, user.getPassword())) {

            JejahitsTokenStore nostraTokenStore = ApplicationContextProvider.getApplicationContext().getBean(JejahitsTokenStore.class);
            String date = new SimpleDateFormat("yyyyMMddHHmmsss").format(new Date());
            String newAccessToken = UUID.nameUUIDFromBytes(user.getUsername().concat(date).getBytes()).toString();
            AccessToken oldToken = accessTokenRepository.findByAccessToken(user.getAccessToken());
            if (oldToken != null) {
                accessTokenRepository.deleteRow(oldToken);
            }
            user.setAccessToken(newAccessToken);
            userRepository.save(user);

            nostraTokenStore.storeAccessToken(newAccessToken, clientIdUser);
//            String fullName = user.getFullName();

            SignInResponseVO signInResponseVO = new SignInResponseVO();
            if(type.equalsIgnoreCase(Constants.TypeUser.VENDOR)){
                Vendor vendor =vendorRepository.findByUser(user);
                if(null==vendor){
                    throw new JejahitsException("Your account has been registered as CUSTOMER");
                }
                signInResponseVO.setFullName(vendor.getVendorName());
                signInResponseVO.setTypeUser(Constants.TypeUser.VENDOR);
                signInResponseVO.setId(vendor.getSecureId());
            }else if(type.equalsIgnoreCase(Constants.TypeUser.CUSTOMER)){
                Customer customer = customerRepository.findByUser(user);
                if(null==customer){
                    throw new JejahitsException("Your account has been registered as VENDOR");
                }
                signInResponseVO.setFullName(customer.getCustomerName());
                signInResponseVO.setTypeUser(Constants.TypeUser.CUSTOMER);
                signInResponseVO.setId(customer.getSecureId());
            }

//            Customer customer = customerRepository.findByUser(user);
//            if(customer==null){
//                Vendor vendor =vendorRepository.findByUser(user);
//                signInResponseVO.setFullName(vendor.getVendorName());
//                signInResponseVO.setTypeUser(Constants.TypeUser.VENDOR);
//                signInResponseVO.setId(vendor.getSecureId());
//            }else{
//                signInResponseVO.setFullName(customer.getCustomerName());
//                signInResponseVO.setTypeUser(Constants.TypeUser.CUSTOMER);
//                signInResponseVO.setId(customer.getSecureId());
//            }

            signInResponseVO.setAccessToken(user.getAccessToken());

//            resultVO.setMessage(StatusCode.OK.name());
//            resultVO.setResult(signInResponseVO);
            return signInResponseVO;

        } else {

//            resultVO.setMessage(StatusCode.ERROR.name());
//            resultVO.setResult("Username atau password tidak valid");

//            return resultVO;
            throw new JejahitsException("Username atau password tidak valid");
        }
    }


    public SignInResponseVO loginUser(SignInRequestVO signInRequest, String type){

        User user = userRepository.findByUsername(signInRequest.getUsername());

        if(null == user) {
            throw new JejahitsException("Username atau password tidak valid");
        }

        if(!user.getActive()){
            throw new JejahitsException("Akun tidak aktif");
        }

        Parameter expired = parameterRepository.findByCode(Constants.ParamCode.PASSWORD);
        int passwordChange = Integer.parseInt(expired.getValue());
        if(user.getImmediateChangePassword() == false){
            Date lastUpdatedDate= user.getLastUpdatedPassword();
            Date today = new Date();
            long diff = today.getTime() - lastUpdatedDate.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if(diffDays > passwordChange){
                user.setImmediateChangePassword(true);
                userRepository.save(user);
            }
        }
//        return null;

        return doLoginUser(signInRequest.getPassword(), user, type);
    }

    public String getUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null) {
            throw new JejahitsException("User is not authenticated");
        }

        return authentication.getName();
    }

    public User getUser(){
        return userRepository.findByUsername(getUsername());
    }

    public Boolean logout(String accessTokenStr) {

        if (StringUtils.isEmpty(accessTokenStr)) throw new JejahitsException("access token wajib terisi");

        AccessToken accessToken = accessTokenRepository.findByAccessToken(accessTokenStr);

        if (null == accessToken) throw new JejahitsException("access token tidak ditemukan");

        accessTokenRepository.delete(accessToken);

        return Boolean.TRUE;
    }

    public Boolean logout(SignOutRequestVO signOutRequestVO) {

        if (StringUtils.isEmpty(signOutRequestVO.getAccessToken())) throw new JejahitsException("access_token is empty");

        AccessToken accessToken = accessTokenRepository.findByAccessToken(signOutRequestVO.getAccessToken());

        if (null == accessToken) throw new JejahitsException("access_token not found");

        accessTokenRepository.delete(accessToken);

        return Boolean.TRUE;
    }

    public Boolean isValid(String accessTokenStr) {

        if (!StringUtils.isEmpty(accessTokenStr)) {
            AccessToken accessToken = accessTokenRepository.findByAccessToken(accessTokenStr);

            if (null != accessToken && !accessToken.getDeleted()){
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

}

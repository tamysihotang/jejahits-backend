package com.jejahits.project.service;

import com.jejahits.project.converter.ProvinsiConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.vo.RequestVO.ProvinsiRequestVO;
import com.jejahits.project.vo.ResponseVO.KecamatanVO;
import com.jejahits.project.vo.ResponseVO.ProvinsiVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class ProvinsiService {

    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    ProvinsiConverter provinsiConverter;

    public Boolean createProvinsi(ProvinsiRequestVO provinsiRequestVO){
        Provinsi provinsi = new Provinsi();
        provinsi.setNamaProvinsi(provinsiRequestVO.getName().toUpperCase());
        provinsiRepository.save(provinsi);
        return Boolean.TRUE;
    }

    public List<ProvinsiVO> listProvinsi(){
        List<ProvinsiVO> provinsiVOS = new ArrayList<>();
        List<Provinsi> provinsiList = provinsiRepository.findAll();
        for(Provinsi provinsi : provinsiList){
            ProvinsiVO provinsiVO = provinsiConverter.transferModelToVO(provinsi,null);
            provinsiVOS.add(provinsiVO);
        }
        return provinsiVOS;
    }

    public Map<String, Object> listOfProvinsi(Integer page, Integer limit,  String namaProvinsi) {
        String sortBy = "namaProvinsi";
        String direction = "asc";

        namaProvinsi = StringUtils.isEmpty(namaProvinsi) ? "%" : "%" + namaProvinsi + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));


        Page<Provinsi> resultPage = provinsiRepository.findByNamaProvinsiLikeIgnoreCase(namaProvinsi, pageable);

        List<Provinsi> models = resultPage.getContent();
        Collection<ProvinsiVO> vos = provinsiConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }
}

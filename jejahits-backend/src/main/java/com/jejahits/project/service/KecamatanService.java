package com.jejahits.project.service;

import com.jejahits.project.converter.KecamatanVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.KecamatanRepository;
import com.jejahits.project.vo.RequestVO.KecamatanRequestVO;
import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import com.jejahits.project.vo.ResponseVO.KecamatanVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class KecamatanService {
    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KecamatanRepository kecamatanRepository;

    @Autowired
    KecamatanVOConverter kecamatanVOConverter;

    public Boolean createKabupaten(KecamatanRequestVO kecamatanRequestVO){
        Kecamatan kecamatan = new Kecamatan();
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(kecamatanRequestVO.getKabupaten().getId());
        if(null==kabupaten){
            throw new JejahitsException("Kabupaten not found");
        }
        kecamatan.setNamaKecamatan(kecamatanRequestVO.getName());
        kecamatan.setKabupaten(kabupaten);
        kecamatanRepository.save(kecamatan);
        return Boolean.TRUE;
    }

    public Map<String, Object> listKecamatan(Integer page, Integer limit, String kabupatenId, String namaKecamatan) {
        String sortBy = "namaKecamatan";
        String direction = "asc";

        namaKecamatan = StringUtils.isEmpty(namaKecamatan) ? "%" : "%" + namaKecamatan + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));

        Kabupaten kabupaten = kabupatenRepository.findBySecureId(kabupatenId);
        if(null==kabupaten){
            throw new JejahitsException("kabupaten not Found");
        }

        Page<Kecamatan> resultPage = kecamatanRepository.findByKabupatenAndNamaKecamatanLikeIgnoreCase(kabupaten, namaKecamatan,pageable);

        List<Kecamatan> models = resultPage.getContent();
        Collection<KecamatanVO> vos = kecamatanVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }
}


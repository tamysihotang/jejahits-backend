package com.jejahits.project.service;

import com.jejahits.project.converter.KategoryVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.repository.KategoryRepository;
import com.jejahits.project.vo.RequestVO.KategoryRequestVO;
import com.jejahits.project.vo.ResponseVO.KategoryVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KategoryService {
    @Autowired
    KategoryRepository kategoryRepository;

    @Autowired
    KategoryVOConverter kategoryVOConverter;

    public Boolean createKategory(KategoryRequestVO kategoryRequestVO){
        Kategory cekKategory = kategoryRepository.findByKategoryName(kategoryRequestVO.getKategoryName().toUpperCase());
        if(cekKategory != null){
            throw new JejahitsException("Kategory Already Exists");
        }
        Kategory kategory = new Kategory();
        kategory.setKategoryName(kategoryRequestVO.getKategoryName().toUpperCase());
        kategoryRepository.save(kategory);
        return Boolean.TRUE;
    }
    public List<KategoryVO> listKategory (Integer page, Integer limit, String kategoryName){
        String sortBy = "kategoryName";
        String direction = "asc";

        kategoryName = StringUtils.isEmpty(kategoryName) ? "%" : "%" + kategoryName + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));
        List<KategoryVO> kategoryVOList = new ArrayList<>();
        Page<Kategory> resultPage = kategoryRepository.findAll(pageable);
        List<Kategory> models = resultPage.getContent();
        kategoryVOConverter.transferListOfModelToListOfVO(models,kategoryVOList);
        return kategoryVOList;
    }
}

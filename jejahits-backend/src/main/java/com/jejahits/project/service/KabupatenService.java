package com.jejahits.project.service;

import com.jejahits.project.converter.KabupatenVOConverter;
import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Customer;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import com.jejahits.project.vo.ResponseVO.CustomerVO;
import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
@Slf4j
@Service
public class KabupatenService {
    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KabupatenVOConverter kabupatenVOConverter;

    public Boolean createKabupaten(KabupatenRequestVO kabupatenRequestVO){
        Kabupaten kabupaten = new Kabupaten();
        Provinsi provinsi = provinsiRepository.findBySecureId(kabupatenRequestVO.getProvinsi().getId());
        if(null==provinsi){
            throw new JejahitsException("Provinsi not found");
        }
        kabupaten.setNamaKabupaten(kabupatenRequestVO.getName());
        kabupaten.setProvinsi(provinsi);
        kabupatenRepository.save(kabupaten);
        return Boolean.TRUE;
    }


    public Map<String, Object> listKabupaten(Integer page, Integer limit, String provinsiId, String namaKabupaten) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getAuthorities();
        log.info("authentication.getAuthorities() "+authentication.getAuthorities());
        String sortBy = "namaKabupaten";
        String direction = "asc";

        namaKabupaten = StringUtils.isEmpty(namaKabupaten) ? "%" : "%" + namaKabupaten + "%";
        Pageable pageable = new PageRequest(page, limit, AbstractBaseVoService.getSortBy(sortBy, direction));

        Provinsi provinsi = provinsiRepository.findBySecureId(provinsiId);
        if(null==provinsi){
            throw new JejahitsException("Provinsi not Found");
        }

        Page<Kabupaten> resultPage = kabupatenRepository.findByProvinsiAndNamaKabupatenLikeIgnoreCase(provinsi, namaKabupaten, pageable);

        List<Kabupaten> models = resultPage.getContent();
        Collection<KabupatenVO> vos = kabupatenVOConverter.transferListOfModelToListOfVO(models, new ArrayList<>());
        return AbstractBaseVoService.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }
}

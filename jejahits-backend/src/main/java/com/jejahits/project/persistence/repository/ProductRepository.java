package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.domain.Product;
import com.jejahits.project.persistence.domain.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends BaseRepository<Product> {
    Page<Product> findByVendor (Vendor vendor, Pageable pageable);
    Page<Product> findBySecureIdNotInAndVendor (List<String> productId, Vendor vendor, Pageable pageable);
    List<Product> findByVendor (Vendor vendor);
    List<Product> findByVendorAndServiceTypeLikeIgnoreCase (Vendor vendor, String serviceType);
    Page<Product> findByKategory (Kategory kategory, Pageable pageable);
    @Query(value = "select distinct vendor.secureId " +
            "from Product " +
            "where KATEGORY_PRODUCT = :kategory")
    List<String> findByKategorynative (@Param("kategory") Kategory kategory);
    Page<Product> findByVendorAndKategory (Vendor vendor, Kategory kategory, Pageable pageable);
    List<Product> findByVendorAndKategory (Vendor vendor, Kategory kategory);
    List<Product> findByKategoryAndServiceTypeAndVendor (Kategory kategory, String serviceType, Vendor vendor);
    List<Product> findByServiceTypeAndVendor (String serviceType, Vendor vendor);


}

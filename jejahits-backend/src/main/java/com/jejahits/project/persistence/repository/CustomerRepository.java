package com.jejahits.project.persistence.repository;


import com.jejahits.project.persistence.domain.Customer;
import com.jejahits.project.persistence.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends BaseRepository<Customer> {
    Page<Customer> findByCustomerNameLikeIgnoreCase (String customerName, Pageable pageable);

    Customer findByUser(User user);

}

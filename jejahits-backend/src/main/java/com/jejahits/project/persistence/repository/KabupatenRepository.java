package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Provinsi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KabupatenRepository extends BaseRepository<Kabupaten>{
    List<Kabupaten> findByProvinsi (Provinsi provinsi);
    Page<Kabupaten> findByProvinsiAndNamaKabupatenLikeIgnoreCase(Provinsi provinsi, String namaKabupaten, Pageable pageable);
}

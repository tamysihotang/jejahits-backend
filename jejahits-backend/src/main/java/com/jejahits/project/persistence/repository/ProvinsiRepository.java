package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Provinsi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;


@Repository
public interface ProvinsiRepository extends  BaseRepository<Provinsi>{
    Page<Provinsi> findByNamaProvinsiLikeIgnoreCase (String namaProvinsi, Pageable pageable);
}

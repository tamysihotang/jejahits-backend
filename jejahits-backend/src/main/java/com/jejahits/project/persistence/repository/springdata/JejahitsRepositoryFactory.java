package com.jejahits.project.persistence.repository.springdata;

import com.jejahits.project.persistence.domain.Base;
import com.jejahits.project.persistence.repository.impl.BaseRepositoryImpl;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;

import javax.persistence.EntityManager;
import java.io.Serializable;

import static org.springframework.data.querydsl.QueryDslUtils.QUERY_DSL_PRESENT;

/**
 * Created by fani on 5/2/15.
 */
public class JejahitsRepositoryFactory extends JpaRepositoryFactory {

//    private EntityManager entityManager;

    public JejahitsRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
//        this.entityManager = entityManager;
    }


    @Override
    protected <T, ID extends Serializable> SimpleJpaRepository<?, ?> getTargetRepository(RepositoryInformation information, EntityManager entityManager) {
        Class<?> repositoryInterface = information.getRepositoryInterface();
        JpaEntityInformation<?, Serializable> entityInformation = getEntityInformation(information.getDomainType());

        if (isQueryDslExecutor(repositoryInterface)) {
            return new QueryDslJpaRepository(entityInformation, entityManager);
        } else if (Base.class.isAssignableFrom(information.getDomainType())) {
            return new BaseRepositoryImpl(entityInformation, entityManager);
        } else {
            return new SimpleJpaRepository(entityInformation, entityManager);
        }
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        if (isQueryDslExecutor(metadata.getRepositoryInterface())) {
            return QueryDslJpaRepository.class;
        } else if (Base.class.isAssignableFrom(metadata.getDomainType())){
            return BaseRepositoryImpl.class;
        } else {
            return SimpleJpaRepository.class;
        }
    }

    @SuppressWarnings({"MethodOverridesPrivateMethodOfSuperclass"})
    private boolean isQueryDslExecutor(Class<?> repositoryInterface) {
        return QUERY_DSL_PRESENT && QueryDslPredicateExecutor.class.isAssignableFrom(repositoryInterface);
    }

}

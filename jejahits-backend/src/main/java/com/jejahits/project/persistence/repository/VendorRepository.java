package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface VendorRepository extends BaseRepository<Vendor> {
    Page<Vendor> findByVendorNameLikeIgnoreCase (String vendorName, Pageable pageable);

    Vendor findByUser (User user);

    Page<Vendor> findBySecureIdInAndVendorNameLikeIgnoreCase(List<String> vendorList, String vendorName, Pageable pageable);
    Page<Vendor> findBySecureIdInAndRatingAndVendorNameLikeIgnoreCase(List<String> vendorList, Integer rating, String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi,String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndRatingAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi,Integer rating, String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndKabupatenAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi, Kabupaten kabupaten,String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndKabupatenAndRatingAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi, Kabupaten kabupaten,Integer rating,String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndKabupatenAndKecamatanAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi, Kabupaten kabupaten, Kecamatan kecamatan,String vendorName,Pageable pageable);
    Page<Vendor> findBySecureIdInAndProvinsiAndKabupatenAndKecamatanAndRatingAndVendorNameLikeIgnoreCase(List<String> vendorList, Provinsi provinsi, Kabupaten kabupaten, Kecamatan kecamatan,Integer rating,String vendorName,Pageable pageable);
}

package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Tamy 13 Okt 2019
 */

@Entity
@Table(name = "PRODUCT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "project")
@DynamicUpdate
@Data
public class Product extends Base {
    @ManyToOne
    @JoinColumn(name = "KATEGORY_PRODUCT", nullable = false)
    private Kategory kategory;

    @ManyToOne
    @JoinColumn(name = "VENDOR", nullable = false)
    private Vendor vendor;

    @Column(name = "HARGA_RANGE_AWAL", nullable = false)
    private BigDecimal hargaRangeAwal;

    @Column(name = "HARGA_RANGE_AKHIR", nullable = false)
    private BigDecimal hargaRangeAkhir;

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "file1")
    private String file1;

    @Column(name = "file2")
    private String file2;

    @Column(name = "file3")
    private String file3;

    @Column(name = "rating")
    private Integer rating;

}

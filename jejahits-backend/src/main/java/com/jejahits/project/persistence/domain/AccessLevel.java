package com.jejahits.project.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "ACCESS_LEVEL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "project")
@DynamicUpdate
@Data
public class AccessLevel extends Base {

        @Column(name = "INSTANCE")
        private String instance;

        @Column(name = "VALUE")
        private String value;

        @Column(name= "KEY_NAME")
        private String keyName;

}

package com.jejahits.project.persistence.repository;

import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface KecamatanRepository extends BaseRepository<Kecamatan> {
    List<Kecamatan> findByKabupaten (Kabupaten kabupaten);
    Page<Kecamatan> findByKabupatenAndNamaKecamatanLikeIgnoreCase(Kabupaten kabupaten, String namaKecamatan, Pageable pageable);
}

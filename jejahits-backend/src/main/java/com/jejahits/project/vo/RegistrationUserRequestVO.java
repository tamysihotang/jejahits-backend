package com.jejahits.project.vo;

import lombok.Data;

import java.util.Date;


@Data
public class RegistrationUserRequestVO {

    //user data
    private String fullName;
    private String email;
    private String username;
    private String password;
//    private String typeUser;
    private String phone;
    private Date tanggalLahir;
    private String jenisKelamin;
    private String provinsi;
    private String kebupaten;
    private String kecamatan;
    private String alamat;



}

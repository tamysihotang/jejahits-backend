package com.jejahits.project.vo.RequestVO;

import lombok.Data;

@Data
public class KategoryRequestVO {
    private String kategoryName;
}

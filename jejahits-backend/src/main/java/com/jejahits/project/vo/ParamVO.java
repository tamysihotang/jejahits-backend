package com.jejahits.project.vo;

import lombok.Data;

@Data
public class ParamVO {
    private String key;
    private Object value;
}

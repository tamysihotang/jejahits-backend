package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ResultVO {

    private String message;
    private Object result;

    public ResultVO() {
    }

    public ResultVO(String message, Object result) {
        this.message = message;
        this.result = result;
    }
}

package com.jejahits.project.vo.RequestVO;

import com.jejahits.project.vo.ResponseVO.ProvinsiVO;
import lombok.Data;

import java.util.List;

@Data
public class KabupatenRequestVO {
    private String name;
    private ProvinsiVO provinsi;
}

package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ParameterVO extends BaseVO{
    String code;
    String value;
    String description;
}

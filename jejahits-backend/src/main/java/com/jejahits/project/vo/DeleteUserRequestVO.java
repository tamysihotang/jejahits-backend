package com.jejahits.project.vo;

import lombok.Data;


@Data
public class DeleteUserRequestVO {
    private String id;
}

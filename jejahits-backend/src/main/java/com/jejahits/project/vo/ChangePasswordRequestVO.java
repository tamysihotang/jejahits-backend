package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ChangePasswordRequestVO {

    private String oldPassword;
    private String newPassword;
    private String confirmationPassword;
}

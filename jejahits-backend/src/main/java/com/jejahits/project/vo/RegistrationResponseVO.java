package com.jejahits.project.vo;

import lombok.Data;

@Data
public class RegistrationResponseVO {

    private String accessToken;
}

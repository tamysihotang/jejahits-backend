package com.jejahits.project.vo.ResponseVO;

import lombok.Data;

@Data
public class ServiceVO {
    private VermakVO vermak;
    private JahitVO jahit;

}

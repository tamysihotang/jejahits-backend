package com.jejahits.project.vo;

import lombok.Data;


@Data
public class PrivilegeNameVO {
    private String name;
}

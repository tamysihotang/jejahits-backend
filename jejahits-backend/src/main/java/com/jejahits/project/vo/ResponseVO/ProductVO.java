package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductVO extends BaseVO {

    private String kategory;
    private String kategoryId;
    private BigDecimal hargaRangeAwal;
    private BigDecimal hargaRangeAkhir;
    private String serviceType;
    private VendorForProductVO vendorVO;
    List<FileUploadVO> image;
    private Integer rating;
}

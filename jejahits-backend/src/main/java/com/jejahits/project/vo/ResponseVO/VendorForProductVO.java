package com.jejahits.project.vo.ResponseVO;

import lombok.Data;

@Data
public class VendorForProductVO {
    private String vendorName;

    private ProvinsiVO provinsi;

    private KabupatenVO kabupaten;

    private KecamatanVO kecamatan;

    private String alamat;

    private Integer rating;

    private String phone;
}

package com.jejahits.project.vo.RequestVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

@Data
public class UpdateRatingVO extends BaseVO {
    private Integer rating;
}

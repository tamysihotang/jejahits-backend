package com.jejahits.project.vo.ResponseVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.util.List;

@Data
public class KecamatanVO extends BaseVO {
    private String namaKecamatan;
}

package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ProfileVO extends ProfileUserVO {

    private String phoneNumber;

}

package com.jejahits.project.vo.ResponseVO;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductVendorVO {
    private String kategory;
    private BigDecimal hargaRangeAwal;
    private BigDecimal hargaRangeAkhir;
    private String serviceType;
    private Integer rating;
    List<FileUploadVO> image;
}

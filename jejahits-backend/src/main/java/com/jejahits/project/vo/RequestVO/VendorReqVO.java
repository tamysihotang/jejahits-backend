package com.jejahits.project.vo.RequestVO;

import com.jejahits.project.vo.BaseVO;
import lombok.Data;

import java.util.Date;

@Data
public class VendorReqVO extends BaseVO {
    private String vendorName;

    private String phone;

    private Date tanggalLahir;

    private String jenisKelamin;

    private String email;

    private String provinsiId;

    private String kabupatenId;

    private String kecamatanId;

    private String username;

    private String alamat;

    private Boolean active;

    private Integer rating;

}

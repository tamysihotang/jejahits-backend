package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ChangeProfileRequestVO extends ChangeProfileUserRequestVO {

    private String phoneNumber;
}

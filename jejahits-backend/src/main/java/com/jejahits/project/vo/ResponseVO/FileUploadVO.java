package com.jejahits.project.vo.ResponseVO;

import lombok.Data;

@Data
public class FileUploadVO {
    private String fileName;
    private String fileUrl;
}

package com.jejahits.project.vo;

import lombok.Data;

@Data
public class UpdateUserRequestVO extends BaseVO{
    String email;
    Boolean active;
    String fullName;
    String phoneNumber;
    String groupId;
}

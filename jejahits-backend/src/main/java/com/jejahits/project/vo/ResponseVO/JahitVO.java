package com.jejahits.project.vo.ResponseVO;

import lombok.Data;

import java.util.Collection;
import java.util.List;

@Data
public class JahitVO {
    private Collection<ProductVO> productVO;
}

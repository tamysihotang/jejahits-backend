package com.jejahits.project.vo;

import lombok.Data;


@Data
public class AccessLevelVO extends BaseVO{
    String instance;
    String value;
    String keyName;
}

package com.jejahits.project.vo.RequestVO;

import lombok.Data;

@Data
public class ChangePasswordReqVO {
    private String email;
    private String newPassword;

}

package com.jejahits.project.vo.RequestVO;

import com.jejahits.project.vo.ResponseVO.FileUploadVO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductRequestVO {
    private String kategoryId;
    private BigDecimal hargaRangeAwal;
    private BigDecimal hargaRangeAkhir;
    private String serviceType;
    List<FileUploadVO> image;
    private Integer rating;

}

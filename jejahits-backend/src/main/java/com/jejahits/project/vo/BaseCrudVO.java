package com.jejahits.project.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseCrudVO extends BaseVO {

    /**
     * Internal ID / Primary Key Domain / Autoincrement
     */
    @JsonIgnore
    private Integer internalId;

    private Date creationDate;

    private String createdBy;

    private Date modificationDate;

    private String modifiedBy;

    private Integer version;

    private Boolean deleted;

}

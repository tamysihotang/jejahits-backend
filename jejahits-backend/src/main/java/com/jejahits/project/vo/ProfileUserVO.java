package com.jejahits.project.vo;

import lombok.Data;


@Data
public class ProfileUserVO{

    private String email;
    private String fullName;
    private String address;

}

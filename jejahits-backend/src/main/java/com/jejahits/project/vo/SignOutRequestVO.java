package com.jejahits.project.vo;

import lombok.Data;


@Data
public class SignOutRequestVO {

    private String accessToken;

}

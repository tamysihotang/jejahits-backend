package com.jejahits.project.vo;

import lombok.Data;
import java.util.Date;


@Data
public class NewNotificationVO{

    private String notificationKey;
    private String url;
    private String message;
    private Date expiredDate;
    private Date creationDate;
}

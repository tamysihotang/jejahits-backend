package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.ResponseVO.KecamatanVO;
import org.springframework.stereotype.Component;

@Component
public class KecamatanVOConverter extends BaseVoConverter<KecamatanVO, KecamatanVO, Kecamatan> implements IBaseVoConverter<KecamatanVO,KecamatanVO, Kecamatan> {
    @Override
    public KecamatanVO transferModelToVO(Kecamatan model, KecamatanVO vo){
        if(null == vo) vo = new KecamatanVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"namaKecamatan"});
        return vo;
    }
}

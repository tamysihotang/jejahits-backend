package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.User;
import com.jejahits.project.vo.DetailUserVO;
import com.jejahits.project.vo.UserVO;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserVoConverter extends BaseVoConverter<UserVO, UserVO, User> implements IBaseVoConverter<UserVO, UserVO, User> {


    @Override
    public User transferVOToModel(UserVO vo, User model) {
        if (null == model) model = new User();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"email","active","username","immediateChangePassword","lastUpdatedDate"});
        return model;
    }

    @Override
    public UserVO transferModelToVO(User model, UserVO vo) {

        if (null == vo) vo = new UserVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"email","active","username","immediateChangePassword","lastUpdatedDate"});
        if(model.getSecureId() != null)vo.setId(model.getSecureId());

        return vo;
    }

    public DetailUserVO transferModelToVONew(User model, DetailUserVO vo) {
        if (null == vo) vo = new DetailUserVO();

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"username","active","groupId","email"});
        vo.setId(model.getSecureId());
        return vo;
    }



}

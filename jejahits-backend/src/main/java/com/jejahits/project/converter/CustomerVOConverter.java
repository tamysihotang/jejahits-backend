package com.jejahits.project.converter;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Customer;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.KecamatanRepository;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.util.JejahitsDateUtils;
import com.jejahits.project.vo.RegistrationUserRequestVO;
import com.jejahits.project.vo.RequestVO.CustomerRequestVO;
import com.jejahits.project.vo.ResponseVO.CustomerVO;
import com.jejahits.project.vo.ResponseVO.KabupatenVO;
import com.jejahits.project.vo.ResponseVO.KecamatanVO;
import com.jejahits.project.vo.ResponseVO.ProvinsiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CustomerVOConverter extends BaseVoConverter<CustomerVO, CustomerVO, Customer> implements IBaseVoConverter<CustomerVO, CustomerVO, Customer>{
    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KecamatanRepository kecamatanRepository;

    @Autowired
    ProvinsiConverter provinsiConverter;

    @Autowired
    KabupatenVOConverter kabupatenVOConverter;

    @Autowired
    KecamatanVOConverter kecamatanVOConverter;

    @Override
    public CustomerVO transferModelToVO(Customer model, CustomerVO vo) {
        if (null == vo) vo = new CustomerVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"customerName", "phone", "tanggalLahir","jenisKelamin","email","alamat","active"});
        String date = JejahitsDateUtils.dateToString(model.getTanggalLahir(),JejahitsDateUtils.YYYY_MM_DD);
        vo.setTanggalLahir(date);
        ProvinsiVO provinsiVO = provinsiConverter.transferModelToVO(model.getProvinsi(),null);
        KabupatenVO kabupatenVO = kabupatenVOConverter.transferModelToVO(model.getKabupaten(),null);
        KecamatanVO kecamatanVO = kecamatanVOConverter.transferModelToVO(model.getKecamatan(),null);
        vo.setProvinsi(provinsiVO);
        vo.setKabupaten(kabupatenVO);
        vo.setKecamatan(kecamatanVO);
        vo.setUsername(model.getUser().getUsername());
        return vo;
    }

    @Override
    public Customer transferVOToModel(CustomerVO vo, Customer model) {
        if (null == model) model = new Customer();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"customerName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active"});
        if (vo.getId() != null) model.setSecureId(vo.getId());
        Provinsi provinsi = provinsiRepository.findBySecureId(vo.getProvinsi().getId());
        if(provinsi == null){
            throw new JejahitsException("Provinsi not Found");
        }
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(vo.getKabupaten().getId());
        if(kabupaten==null){
            throw new JejahitsException("Kabupaten not Found");
        }
        Kecamatan kecamatan = kecamatanRepository.findBySecureId(vo.getKecamatan().getId());
        if(kecamatan == null){
            throw new JejahitsException("Kecamatan not Found");
        }
        model.setProvinsi(provinsi);
        model.setKabupaten(kabupaten);
        model.setKecamatan(kecamatan);
        return model;
    }


    public Customer transferVOToModel(CustomerRequestVO vo, Customer model) {
        if (null == model) model = new Customer();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"customerName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active"});
        if (vo.getId() != null) model.setSecureId(vo.getId());
        Provinsi provinsi = provinsiRepository.findBySecureId(vo.getProvinsiId());
        if(provinsi == null){
            throw new JejahitsException("Provinsi not Found");
        }
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(vo.getKabupatenId());
        if(kabupaten==null){
            throw new JejahitsException("Kabupaten not Found");
        }
        Kecamatan kecamatan = kecamatanRepository.findBySecureId(vo.getKecamatanId());
        if(kecamatan == null){
            throw new JejahitsException("Kecamatan not Found");
        }
        model.setProvinsi(provinsi);
        model.setKabupaten(kabupaten);
        model.setKecamatan(kecamatan);
        return model;
    }

    public Customer transferVOToModel(RegistrationUserRequestVO vo, Customer model, String secureId) {
        if (null == model) model = new Customer();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active"});
        model.setSecureId(secureId);
        model.setDeleted(Boolean.FALSE);
        return model;
    }

}

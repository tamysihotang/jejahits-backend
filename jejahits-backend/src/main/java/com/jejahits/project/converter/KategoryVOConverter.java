package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.ResponseVO.KategoryVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KategoryVOConverter extends BaseVoConverter<KategoryVO, KategoryVO, Kategory> implements IBaseVoConverter<KategoryVO,KategoryVO, Kategory> {
    @Override
    public KategoryVO transferModelToVO(Kategory model, KategoryVO vo){
        if(null == vo) vo = new KategoryVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"kategoryName","image"});
        log.info(model.getImage());
        return vo;
    }
}

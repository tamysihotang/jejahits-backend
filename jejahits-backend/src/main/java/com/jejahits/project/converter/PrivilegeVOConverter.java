package com.jejahits.project.converter;

import com.jejahits.project.persistence.domain.Privilege;
import com.jejahits.project.vo.PrivilegeNameVO;
import com.jejahits.project.vo.PrivilegeVO;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;


@Component
public class PrivilegeVOConverter extends BaseVoConverter<PrivilegeVO, PrivilegeVO, Privilege> implements IBaseVoConverter<PrivilegeVO, PrivilegeVO, Privilege> {

    @Override
    public Privilege transferVOToModel(PrivilegeVO vo, Privilege model) {
        if (null == model) model = new Privilege();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"name","description"});
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }


    public PrivilegeVO transferModelToVO(Privilege model, PrivilegeVO vo,Boolean value) {

        if (null == vo) vo = new PrivilegeVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name", "description"});
        vo.setValue(value);
        return vo;
    }

    public PrivilegeNameVO transferModelToVOSome(Privilege model, PrivilegeNameVO vo) {
        if (null == vo) vo = new PrivilegeNameVO();
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"name"});
        return vo;
    }

    public Collection<PrivilegeVO> transferListOfModelToListOfVO(Collection<Privilege> models, Collection<PrivilegeVO> vos,Boolean value) {
        if (null == vos) vos = new ArrayList<>();
        for (Privilege model : models) {
            PrivilegeVO vo = transferModelToVO(model, null, value);
            vos.add(vo);
        }
        return vos;
    }

    public Collection<PrivilegeNameVO> transferListOfModelToListOfVOOnly(Collection<Privilege> models, Collection<PrivilegeNameVO> vos) {
    if (null == vos) vos = new ArrayList<>();
        for (Privilege model : models) {
            PrivilegeNameVO vo = transferModelToVOSome(model, null);
            vos.add(vo);
        }
        return vos;
    }
}

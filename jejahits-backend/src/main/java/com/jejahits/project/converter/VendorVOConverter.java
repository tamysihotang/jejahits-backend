package com.jejahits.project.converter;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kabupaten;
import com.jejahits.project.persistence.domain.Kecamatan;
import com.jejahits.project.persistence.domain.Provinsi;
import com.jejahits.project.persistence.domain.Vendor;
import com.jejahits.project.persistence.repository.KabupatenRepository;
import com.jejahits.project.persistence.repository.KecamatanRepository;
import com.jejahits.project.persistence.repository.ProvinsiRepository;
import com.jejahits.project.util.DateUtils;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.util.JejahitsDateUtils;
import com.jejahits.project.vo.RegistrationUserRequestVO;
import com.jejahits.project.vo.RequestVO.UpdateRatingVO;
import com.jejahits.project.vo.RequestVO.VendorReqVO;
import com.jejahits.project.vo.ResponseVO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class VendorVOConverter extends BaseVoConverter<VendorVO, VendorVO, Vendor> implements IBaseVoConverter<VendorVO, VendorVO, Vendor> {
    @Autowired
    ProvinsiRepository provinsiRepository;

    @Autowired
    KabupatenRepository kabupatenRepository;

    @Autowired
    KecamatanRepository kecamatanRepository;

    @Autowired
    ProvinsiConverter provinsiConverter;

    @Autowired
    KabupatenVOConverter kabupatenVOConverter;

    @Autowired
    KecamatanVOConverter kecamatanVOConverter;


    @Override
    public VendorVO transferModelToVO(Vendor model, VendorVO vo) {
        if (null == vo) vo = new VendorVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email","alamat","active","rating"});
        String date = JejahitsDateUtils.dateToString(model.getTanggalLahir(),JejahitsDateUtils.YYYY_MM_DD);
        vo.setTanggalLahir(date);
        ProvinsiVO provinsiVO = provinsiConverter.transferModelToVO(model.getProvinsi(),null);
        KabupatenVO kabupatenVO = kabupatenVOConverter.transferModelToVO(model.getKabupaten(),null);
        KecamatanVO kecamatanVO = kecamatanVOConverter.transferModelToVO(model.getKecamatan(),null);
        vo.setProvinsi(provinsiVO);
        vo.setKabupaten(kabupatenVO);
        vo.setKecamatan(kecamatanVO);
        vo.setUsername(model.getUser().getUsername());
        return vo;
    }

    public VendorForProductVO transferModelToVOVendor(Vendor model, VendorForProductVO vo) {
        if (null == vo) vo = new VendorForProductVO();

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email","alamat","active","rating"});
        String date = JejahitsDateUtils.dateToString(model.getTanggalLahir(),JejahitsDateUtils.YYYY_MM_DD);
        ProvinsiVO provinsiVO = provinsiConverter.transferModelToVO(model.getProvinsi(),null);
        KabupatenVO kabupatenVO = kabupatenVOConverter.transferModelToVO(model.getKabupaten(),null);
        KecamatanVO kecamatanVO = kecamatanVOConverter.transferModelToVO(model.getKecamatan(),null);
        vo.setProvinsi(provinsiVO);
        vo.setKabupaten(kabupatenVO);
        vo.setKecamatan(kecamatanVO);
        return vo;
    }

    @Override
    public Vendor transferVOToModel(VendorVO vo, Vendor model) {
        if (null == model) model = new Vendor();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active"});
        Provinsi provinsi = provinsiRepository.findBySecureId(vo.getProvinsi().getId());
        if(provinsi == null){
            throw new JejahitsException("Provinsi not Found");
        }
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(vo.getKabupaten().getId());
        if(kabupaten==null){
            throw new JejahitsException("Kabupaten not Found");
        }
        Kecamatan kecamatan = kecamatanRepository.findBySecureId(vo.getKecamatan().getId());
        if(kecamatan == null){
            throw new JejahitsException("Kecamatan not Found");
        }
        model.setProvinsi(provinsi);
        model.setKabupaten(kabupaten);
        model.setKecamatan(kecamatan);
        return model;
    }

    public Vendor transferVOToModel(VendorReqVO vo, Vendor model) {
        if (null == model) model = new Vendor();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active"});
        Provinsi provinsi = provinsiRepository.findBySecureId(vo.getProvinsiId());
        if(provinsi == null){
            throw new JejahitsException("Provinsi not Found");
        }
        Kabupaten kabupaten = kabupatenRepository.findBySecureId(vo.getKabupatenId());
        if(kabupaten==null){
            throw new JejahitsException("Kabupaten not Found");
        }
        Kecamatan kecamatan = kecamatanRepository.findBySecureId(vo.getKecamatanId());
        if(kecamatan == null){
            throw new JejahitsException("Kecamatan not Found");
        }
        model.setProvinsi(provinsi);
        model.setKabupaten(kabupaten);
        model.setKecamatan(kecamatan);
        return model;
    }

    public Vendor transferVOToModel(RegistrationUserRequestVO vo, Vendor model, String secureId) {
        if (null == model) model = new Vendor();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active","rating"});
        model.setSecureId(secureId);
        model.setDeleted(Boolean.FALSE);
        return model;
    }

    public Vendor transferVOToModel(UpdateRatingVO vo, Vendor model) {
        if (null == model) model = new Vendor();
        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"vendorName", "phone", "tanggalLahir","jenisKelamin","email", "provinsi","kabupaten", "kecamatan","alamat","active","rating"});
        return model;
    }
}

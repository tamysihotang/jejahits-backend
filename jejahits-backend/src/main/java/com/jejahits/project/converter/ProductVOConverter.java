package com.jejahits.project.converter;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.persistence.domain.Kategory;
import com.jejahits.project.persistence.domain.Product;
import com.jejahits.project.persistence.domain.Vendor;
import com.jejahits.project.persistence.repository.KategoryRepository;
import com.jejahits.project.persistence.repository.VendorRepository;
import com.jejahits.project.util.Constants;
import com.jejahits.project.util.ExtendedSpringBeanUtil;
import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import com.jejahits.project.vo.ResponseVO.FileUploadVO;
import com.jejahits.project.vo.ResponseVO.ProductVO;
import com.jejahits.project.vo.ResponseVO.VendorForProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductVOConverter extends BaseVoConverter<ProductVO, ProductVO, Product> implements IBaseVoConverter<ProductVO,ProductVO, Product> {
    @Autowired
    KategoryRepository kategoryRepository;

    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    VendorVOConverter vendorVOConverter;

    @Override
    public Product transferVOToModel(ProductVO vo, Product model) {
        if (null == model) model = new Product();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"kategory","vendor","hargaRangeAwal","hargaRangeAkhir","serviceType","rating"});
        if(vo.getId() != null)model.setSecureId(vo.getId());
        return model;
    }


    @Override
    public ProductVO transferModelToVO(Product model, ProductVO vo) {

        if (null == vo) vo = new ProductVO();
        super.transferModelToVO(model, vo);
        ExtendedSpringBeanUtil.copySpecificProperties(model, vo,
                new String[]{"kategory","vendor","hargaRangeAwal","hargaRangeAkhir","serviceType","rating"});
        List<FileUploadVO> fileUploadVOS =new ArrayList<>();
        if(model.getFile1()!=null){
            FileUploadVO fileUploadVO = new FileUploadVO();
            fileUploadVO.setFileName(model.getFile1());
            fileUploadVO.setFileUrl(model.getFile1());
            fileUploadVOS.add(fileUploadVO);
        }
        if(model.getFile2()!=null){
            FileUploadVO fileUploadVO = new FileUploadVO();
            fileUploadVO.setFileName(model.getFile2());
            fileUploadVO.setFileUrl(model.getFile2());
            fileUploadVOS.add(fileUploadVO);
        }
        if(model.getFile3()!=null){
            FileUploadVO fileUploadVO = new FileUploadVO();
            fileUploadVO.setFileName(model.getFile3());
            fileUploadVO.setFileUrl(model.getFile3());
            fileUploadVOS.add(fileUploadVO);
        }
        vo.setImage(fileUploadVOS);
        Vendor vendor = vendorRepository.findBySecureId(model.getVendor().getSecureId());
        Kategory kategory = kategoryRepository.findBySecureId(model.getKategory().getSecureId());
        vo.setKategory(kategory.getKategoryName());
        vo.setKategoryId(kategory.getSecureId());
        VendorForProductVO vendorVO = new VendorForProductVO();
        vendorVOConverter.transferModelToVOVendor(vendor,vendorVO);
        vo.setVendorVO(vendorVO);
        return vo;
    }

    public Product transferVOToModel(ProductRequestVO vo, Product model) {
        if (null == model) model = new Product();

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model,
                new String[]{"kategory","vendor","hargaRangeAwal","hargaRangeAkhir","serviceType","rating"});
        Kategory kategory = kategoryRepository.findBySecureId(vo.getKategoryId());
        if(kategory == null){
            throw new JejahitsException("Kategory Not Found");
        }
        model.setRating(0);
        model.setKategory(kategory);
        model.setServiceType(vo.getServiceType().toUpperCase());
        return model;
    }
}

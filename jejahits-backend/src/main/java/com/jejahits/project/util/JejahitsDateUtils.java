package com.jejahits.project.util;

import com.jejahits.project.exception.JejahitsException;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class JejahitsDateUtils {

    public static final String YEAR_FORMAT = "yyyy";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MMM_DD = "yyyy-MMM-dd";
    public final static String DD_MMM_YYYY = "dd-MMM-yyyy";
    public final static String DDMMYY = "dd/MM/yy";
    public final static String DDMMYYYY = "ddMMyyyy";
    public final static String DDMMYYHHMMSS = "dd/MM/yy – HH:mm:ss";
    public final static String DD_MM_YYYY = "dd-MM-yyyy";
    public final static String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
    public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public final static String YYYYMMDD = "yyyyMMdd";
    final static String DEFAULT_FORMAT = "yyyy-MMM-dd";
    private static final Logger log = LoggerFactory.getLogger(org.apache.commons.lang.time.DateUtils.class);

    public static String getToday(Date date) {
        return new SimpleDateFormat(DEFAULT_FORMAT).format(date);
    }

    public static Date getMinDate() {
        Date result = null;
        try {
            result = new SimpleDateFormat(DD_MM_YYYY).parse("01-01-1970");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Date getTomorrow() {
        return addDays(new Date(), 1);
    }

    public static Date getTomorrow(Date today) {
        return addDays(today, 1);
    }

//    public static Date getTomorrow(Date today) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(DateUtils.truncate(today, Calendar.DATE));
//        calendar.add(Calendar.DAY_OF_YEAR, 1);
//
//        return calendar.getTime();
//    }

    public static Date getDate(Date date) {
        return DateUtils.truncate(date, Calendar.DATE);
    }

    public static Date addDays(Date date, Integer days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return DateUtils.truncate(calendar.getTime(), Calendar.DATE);
    }


    public static String dateToString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String result = dateFormat.format(date);
        return result;
    }

    public static Date getToday() {
        return getDate(new Date());
    }

    public static String dateToString(Date date) {
        return dateToString(date, "yyyy-MMM-dd");
    }

    public static Date stringToDate(String date, String format) {

        Date result;
        DateFormat dateFormat = new SimpleDateFormat(format);

        try {
            result = dateFormat.parse(date);
        } catch (ParseException e) {
            throw new JejahitsException("Format Date " + DEFAULT_FORMAT + " ex: " + dateToString(new Date()));
        }

        return result;

    }

    private static int count(long start, Date end) {
        DateTime startDate = new DateTime(start);
        log.debug("DEBUG: startDate={}", startDate);
        DateTime endDate = new DateTime(end);
        log.debug("DEBUG: endDate={}", endDate);

        log.debug("DEBUG: daysBetween={}",
                Days.daysBetween(startDate.toLocalDate(), endDate.toLocalDate()).getDays());
        return Days.daysBetween(startDate.toLocalDate(), endDate.toLocalDate()).getDays();
    }

    public static int countDaysSince(long start) {
        return count(start, getToday());
    }

    private static int count(Date start, Date end) {
        DateTime startDate = new DateTime(start);
        log.debug("DEBUG: startDate={}", startDate);
        DateTime endDate = new DateTime(end);
        log.debug("DEBUG: endDate={}", endDate);

        log.debug("DEBUG: daysBetween={}",
                Days.daysBetween(startDate.toLocalDate(), endDate.toLocalDate()).getDays());
        return Days.daysBetween(startDate.toLocalDate(), endDate.toLocalDate()).getDays();
    }

    public static int countDaysSince(Date start) {
        return count(start, getToday());
    }

    public static int countDaysSince(Date start, Date end) {
        return count(start, end);
    }

    public static Date getYesterday() {
        return addDays(new Date(), -1);
    }

    public static Date getYesterday(Date today) {
        return addDays(today, -1);
    }
}

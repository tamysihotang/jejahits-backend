package com.jejahits.project.util;

import org.apache.commons.lang.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by derryaditiya on 6/21/16.
 */
public class PasswordUtil {

    private static final String SYMBOL = "!@#$%&?-+=_";

    public static String generatePassword(){
        String letter = RandomStringUtils.randomAlphabetic(4).toLowerCase();
        String number = RandomStringUtils.randomNumeric(2);
        String symbol = RandomStringUtils.random(2, SYMBOL);

        String password = shuffle(letter+number+symbol);

        return password;
    }

    private static String shuffle(String input){
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }


}

package com.jejahits.project.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class DocumentUtils {

    private static final Logger logger = LoggerFactory.getLogger(DocumentUtils.class);

    @Value("${file.path}")
    String path;

    @Value("${file.url.path}")
    String urlpath;

    @Value("${file.downloadPath}")
    String softlinkPath;

    public String createFolder(String documentPath) {
        String fullPath = path + documentPath;
        return createFolderOnly(fullPath);
    }

    public String createFolderOnly(String documentPath) {
        String response = null;
        File file = new File(documentPath);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                response = "Failed Create Folder !";
            }
        }
        return response;
    }


    public String deleteDocuments(String documentPath){
        String fullPath = path + documentPath;
        return deleteFile(fullPath);
    }

    public String renameOrMoveDocument(String currentPath, String futurePath,String type){
        String response = null;
        String currentFullPath = path + currentPath;
        String futureFullPath = path + futurePath;
        if(type.equalsIgnoreCase("FOLDER")){
            renameOrMoveFolder(currentFullPath,futureFullPath);
        }else {
            renameOrMoveFile(currentPath,futurePath);
        }
        return response;
    }

    public String renameOrMoveFolder(String currentPath, String futurePath){
        String response = null;
        try {
            FileUtils.moveDirectory(
                    FileUtils.getFile(currentPath),
                    FileUtils.getFile(futurePath));
        } catch (IOException e) {
            logger.info("ERROR : " + e.getMessage());
            response = "Failed Renaming / Moving!";
        }
        return response;
    }


    public String renameOrMoveFile(String currentPath, String futurePath){
        String response = null;
        try {
            FileUtils.moveFile(
                    FileUtils.getFile(currentPath),
                    FileUtils.getFile(futurePath)
            );
        } catch (IOException e) {
            logger.info("ERROR : " + e.getMessage());
            response = "ERROR";
        }
        return response;
    }

    public String uploadFile(MultipartFile sourceFile,String destinationPath){
        String currentFullPath = path + destinationPath;
        logger.info("currentFullPath "+currentFullPath);
//        String response = path;
        String response = urlpath;
        try{
            byte[] bytes = sourceFile.getBytes();
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(currentFullPath));
            stream.write(bytes);
            stream.close();
        }catch (Exception e){
            response = "ERROR";
        }
        return response;
    }


    public String downloadFile(String masterPath,String downloadPath,String folderName){
        String currentPath = path + masterPath;
        String destinationPath = softlinkPath + downloadPath;
        String response;
        File folder = new File(softlinkPath+folderName);
        if(!folder.exists()) {
            folder.mkdir();
        }
            Path records = Paths.get(currentPath);
        Path recordsLink = Paths.get(destinationPath);
        try {
            Files.createSymbolicLink(recordsLink,records);
            response = destinationPath;
        } catch (IOException e) {
            logger.info("ERROR : " + e.getMessage());
            response = "ERROR";
        }
        return response;
    }


    private String deleteFile(String fullPath){
        String response = null;
        File file = new File(fullPath);
        try {
            if (!file.isDirectory()) file.delete();
            else FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            logger.info("ERROR : " + e.getMessage());
            response = "Failed Deleting!";
        }
        return response;
    }

    public String cleanDirectorySoftlink(){
        String response = null;
        File folder = new File(softlinkPath);

        try {
            FileUtils.cleanDirectory(folder);
        } catch (Exception e){
            logger.error(e.getMessage());
            response = "Failed Clean Directory Softlink";
        }
        return response;
    }

}

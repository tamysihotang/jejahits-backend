package com.jejahits.project.util;

import com.jejahits.project.exception.JejahitsException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by antin on 1/7/16.
 */
@Component
public class ErrorUtil {

    private static String notNull;

    private static String notFound;

    private static String isExist;

    private static String notEligible;

    @Value("${error.not.null}")
    public void setNotNull(String notNull) {
       ErrorUtil.notNull = notNull;
    }

    @Value("${error.not.found}")
    public void setNotFound(String notFound) {
        ErrorUtil.notFound = notFound;
    }

    @Value("${error.is.exist}")
    public void setIsExist(String isExist) {
        ErrorUtil.isExist = isExist;
    }

    @Value("${error.not.eligible}")
    public void setNotEligible(String notEligible) {
        ErrorUtil.notEligible = notEligible;
    }

    public static JejahitsException notNullError(String var) {
        return new JejahitsException(String.format(notNull, var));
    }

    public static JejahitsException notFoundError(String var) {
        return new JejahitsException(String.format(notFound, var));
    }

    /**
     *  You cant't add, the %s data with %s is %s already exist. You should be execute edit action.
     * @return
     */
    public static JejahitsException isExist(String identifier, String field, String value) {
        return new JejahitsException(String.format(isExist, identifier, field, value));
    }

    public static JejahitsException notEligible(String var) {
        return new JejahitsException(String.format(notEligible, var));
    }

    public static JejahitsException notAuthorize() {
        return new JejahitsException("You're not authorize.");
    }
}

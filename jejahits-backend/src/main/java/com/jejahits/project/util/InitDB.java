package com.jejahits.project.util;

import com.jejahits.project.oauth.provider.UserPasswordAuthenticationToken;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import com.jejahits.project.persistence.domain.AccessLevel;
import com.jejahits.project.persistence.domain.Privilege;
import com.jejahits.project.persistence.repository.AccessLevelRepository;
import com.jejahits.project.persistence.repository.PrivilegeRepository;
import com.jejahits.project.persistence.domain.*;
import com.jejahits.project.persistence.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;

/**
 * agus w on 3/3/16.
 */
@Component
public class InitDB {

    public static final Logger logger = LoggerFactory.getLogger(InitDB.class);

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    AccessLevelRepository accessLevelRepository;

    @Autowired
    UserRepository userRepository;


    @Autowired
    @Qualifier("passwordEncoder")
    PasswordEncoder passwordEncoder;

    @Value("#{'${params.code}'.split(',')}") List<String> paramsCode;

    @Value("#{'${params.desc}'.split(',')}") List<String> paramsDesc;

    @Value("#{'${params.value}'.split(',')}") List<String> paramsValue;

    @Value("#{'${privilege.accessLevelUser}'.split(',')}") List<String> accessLevelUser;

    @Value("#{'${privilege.accessLevelUserDesc}'.split(',')}") List<String>  accessLevelUserDesc;

    @Value("#{'${privilege.accessLevelGroup}'.split(',')}") List<String>  accessLevelGroup;

    @Value("#{'${privilege.accessLevelGroupDesc}'.split(',')}") List<String>  accessLevelGroupDesc;

    @Value("#{'${privilege.accessLevelReport}'.split(',')}") List<String>  accessLevelReport;

    @Value("#{'${privilege.accessLevelReportDesc}'.split(',')}") List<String>  accessLevelReportDesc;

    @Value("#{'${accessLevel.desc}'.split(',')}") List<String> accessLevelDesc;

    @Value("#{'${accessLevel.value}'.split(',')}") List<String> accessLevelValue;

    @Value("#{'${accessLevel.keyName}'.split(',')}") List<String> accessLevelKeyName;

    @Value("${admin.password}") String adminPassword;

    @Value("${admin.email}") String adminEmail;

    @Value("${admin.fullname}") String adminFullname;

    @Value("${admin.name}") String adminName;

    @Value("${group.name}") String groupName;

    @Value("${group.accessLevel}") String groupAccessLevel;

    @Value("${client.id.admin}") String clientIdAdmin;

    @PostConstruct
    public void init(){

        Authentication authentication = new UserPasswordAuthenticationToken("SYSTEM", "SYSTEM");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        createPrivileges();
        initAccessLevel();

        logger.info("---------------------------------------------------------------");
        logger.info("PARAMETER SIZE : {} + {} + {} ", paramsCode.size(),  paramsDesc.size(), paramsValue.size());
        for(int i=0; i<paramsCode.size(); i++){
            initParameter(paramsCode.get(i), paramsDesc.get(i), paramsValue.get(i));
        }
        logger.info("---------------------------------------------------------------");
//        createAdmin();
    }



    private void initAccessLevel(){

        List<AccessLevel> accessLevel = accessLevelRepository.findAll();
        if(accessLevel.size() == 0 ){
            logger.info("CREATE ACCESS LEVEL : {} + {} ", accessLevelDesc.size(),  accessLevelValue.size());
            if(accessLevelDesc.size() == accessLevelValue.size()){

                int[] idx = { 0 };
                accessLevelDesc.forEach(item -> {
                    AccessLevel newAccessLevel = new AccessLevel();
                    newAccessLevel.setInstance(item);
                    newAccessLevel.setValue(accessLevelValue.get(idx[0]));
                    newAccessLevel.setKeyName(accessLevelKeyName.get(idx[0]));
                    idx[0]++;
                    accessLevelRepository.saveAndFlush(newAccessLevel);
                });
            }else{
                logger.info("ACCESS LEVEL DESC AND VALUE NOT MATCH !!!");
            }
        }
    }

    private void initParameter(String code, String desc, String value) {
            logger.info("Checking Parameter code {}, desc {}, value {}", code, desc, value);

        Parameter parameter = parameterRepository.findByCode(code);

        if(null == parameter) {
            parameter = new Parameter();
        }
            logger.info("Creating Parameter code {}, desc {}, value {}", code, desc, value);
            parameter.setCode(code);
            parameter.setDescription(desc);
            parameter.setValue(value);
            parameterRepository.save(parameter);

        logger.info("Checking Parameter code {}, desc {}, value {} Done", code, desc, value);
    }

    private void createPrivileges() {
        //privilege user
        int[] idx = { 0 };
        accessLevelUser.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null){
                privileges = new Privilege();
            }
            privileges.setName(privilege);
            privileges.setDescription(accessLevelUserDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_USER);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

        //privilege group
        idx[0] = 0;
        accessLevelGroup.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null) {
                privileges = new Privilege();
            }
            privileges.setDescription(accessLevelGroupDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_GROUP);
            privileges.setName(privilege);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

        //privilege report
        idx[0] = 0;
        accessLevelReport.forEach((privilege) -> {
            Privilege privileges = privilegeRepository.findByName(privilege);
            if(privileges == null) {
                privileges = new Privilege();
            }
            privileges.setDescription(accessLevelReportDesc.get(idx[0]));
            privileges.setCategory(Constants.PrivilegeCategory.PRIVILEGE_REPORT);
            privileges.setName(privilege);
            idx[0]++;
            privilegeRepository.saveAndFlush(privileges);
        });

    }

}

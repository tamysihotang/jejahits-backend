package com.jejahits.project.oauth.provider;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.service.SignInService;
import com.jejahits.project.util.Constants;
import com.jejahits.project.vo.SignInRequestVO;
import com.jejahits.project.vo.SignInResponseVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * agus w
 */
public class JejahitsUserAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(JejahitsUserAuthenticationProvider.class);

    private SignInService signInService;
    private String clientIdUser;
    private String clientIdAdmin;
    private String clientIdClient;

    private String clientRoleAdmin;
    private String clientRoleUser;
    private String clientRoleClient;

    public void setSignInService(SignInService signInService) {
        this.signInService = signInService;
    }

    public void setClientIdUser(String clientIdUser) {
        this.clientIdUser = clientIdUser;
    }

    public void setClientIdAdmin(String clientIdAdmin) {
        this.clientIdAdmin = clientIdAdmin;
    }

    public void setClientRoleAdmin(String clientRoleAdmin) {
        this.clientRoleAdmin = clientRoleAdmin;
    }

    public void setClientRoleUser(String clientRoleUser) {
        this.clientRoleUser = clientRoleUser;
    }

    public void setClientIdClient(String clientIdClient) {
        this.clientIdClient = clientIdClient;
    }

    public void setClientRoleClient(String clientRoleClient) {
        this.clientRoleClient = clientRoleClient;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        if (null != authentication && null != authentication.getDetails()) {

            Object authenticationDetails = authentication.getDetails();

            //from /oauth/token
            if (authenticationDetails instanceof Map && null != ((Map) authenticationDetails).get("client_id")) {
                return authenticateFromOauthToken((String)((Map) authenticationDetails).get("client_id"), authentication.getPrincipal(), authentication.getCredentials());

            } else if (authenticationDetails instanceof WebAuthenticationDetails) {
                return authenticateFromOauthToken(clientIdAdmin, authentication.getPrincipal(), authentication.getCredentials());
            }
        }

        LOGGER.info("[USER] " + authentication.getPrincipal().toString() + " is not found");
        LOGGER.error("[USER] " + authentication.getPrincipal().toString() + " is not found");
        throw new BadCredentialsException("Bad User Credentials.");
    }

    private Authentication authenticateFromOauthToken(String clientId, Object principal, Object credential) {
        String username = principal.toString();
        String password = credential.toString();

        SignInRequestVO signInRequest = new SignInRequestVO();
//        signInRequest.setEmail(username);
        signInRequest.setUsername(username);
        signInRequest.setPassword(password);

        SignInResponseVO signInResponse = null;

        String type = "";


        if (clientId.compareTo(clientIdUser) == 0) {

            LOGGER.debug("Finding Elearning USER with username " + username + "...");

            try {
//                signInResponse = (SignInResponseVO) signInService.loginUser(signInRequest).getResult();
                signInResponse = signInService.loginUser(signInRequest, type);
            } catch (JejahitsException e) {
                throw new BadCredentialsException(e.getMessage());
            }

            if (null != signInResponse) {

                LOGGER.debug("[USER] " + username + " is found");

                return new UsernamePasswordAuthenticationToken(
                        principal, credential,
                        getRoleClient(clientRoleUser));
            }

        } else if (clientId.compareTo(clientIdClient) == 0) {

            LOGGER.debug("Finding Elearning CLIENT with username " + username + "...");

            try {
                signInResponse =  signInService.loginUser(signInRequest, type);
//                signInResponse = (SignInResponseVO) signInService.loginUser(signInRequest).getResult();
            } catch (JejahitsException e) {
                throw new BadCredentialsException(e.getMessage());
            }

            if (null != signInResponse) {

                LOGGER.debug("[CLIENT] " + username + " is found");

                return new UsernamePasswordAuthenticationToken(
                        principal, credential,
                        getRoleClient(clientRoleClient));
            }

        }

        LOGGER.error("[USER] " + principal.toString() + " is not found");
        throw new BadCredentialsException("Bad User Credentials.");
    }

    private List<GrantedAuthority> getRoleClient(String clientRole) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

        grantedAuthorities.add(new SimpleGrantedAuthority(clientRole));

        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return true;
    }

}

package com.jejahits.project.oauth.provider;

import com.jejahits.project.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.List;


public class JejahitsClientDetailService implements ClientDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(JejahitsClientDetailService.class);

    private String clientIdAdmin;
    private String clientIdUser;

    private String clientRoleAdmin;
    private String clientRoleUser;

    public JejahitsClientDetailService() {
    }

    public void setClientIdAdmin(String clientIdAdmin) {
        this.clientIdAdmin = clientIdAdmin;
    }


    public void setClientRoleAdmin(String clientRoleAdmin) {
        this.clientRoleAdmin = clientRoleAdmin;
    }

    public void setClientIdUser(String clientIdUser) {
        this.clientIdUser = clientIdUser;
    }

    public void setClientRoleUser(String clientRoleUser) {
        this.clientRoleUser = clientRoleUser;
    }

    public ClientDetails loadClientByClientId(final String clientId)
            throws OAuth2Exception {

        logger.debug("FINDING Client_Id {}", clientId);

        if (null == clientId) {
            logger.error("Client_Id NULL");
            throw new InvalidClientException("Client null");
        }

        BaseClientDetails details = null;
        String role = null;
        try {

            if (clientId.compareTo(clientIdAdmin) == 0) {
                role = clientRoleAdmin;
            } else if (clientId.compareTo(clientIdUser) == 0) {
                role = clientRoleUser;
            }

            details = new BaseClientDetails(clientId, "nostra", "TRUST", "password", role, null);

            List<String> authorizedGrantTypes = StringUtils.extractStringToList("password", ",");
            details.setAuthorizedGrantTypes(authorizedGrantTypes);

            List<String> scopes = StringUtils.extractStringToList("TRUST", ",");
            details.setScope(scopes);

            details.setClientId(clientId);
            details.setClientSecret("nostra");
//            details.setAccessTokenValiditySeconds(10000);
//            details.setRefreshTokenValiditySeconds(10000);

            details.setResourceIds(StringUtils.extractStringToList("nostra", ","));


        } catch (EmptyResultDataAccessException e) {
            logger.error("NOT FOUND Client_Id {} ", clientId);
            throw new InvalidClientException("Client not found: " + clientId);
        }

        return details;
    }
}

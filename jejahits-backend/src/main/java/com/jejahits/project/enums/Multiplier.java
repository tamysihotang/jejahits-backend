package com.jejahits.project.enums;

import com.jejahits.project.util.Constants;

/**
 * agus w on 3/1/16.
 */
public enum Multiplier implements BaseModelEnum<Integer> {
    MINUS(Constants.Multiplier.MINUS),PLUS(Constants.Multiplier.PLUS);

    Integer multiplier;

    Multiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public Integer getInternalValue() {
        if(multiplier == 0){
            multiplier = -1;
        }
        return multiplier;
    }
}

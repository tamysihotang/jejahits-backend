package com.jejahits.project.web;

import com.jejahits.project.service.KabupatenService;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/kabupaten")
public class KabupatenController {
    @Autowired
    KabupatenService kabupatenService;

    @ApiOperation(value = "Create Kabupaten")
    @RequestMapping(value = "/create-kabupaten",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KabupatenRequestVO kabupatenRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kabupatenService.createKabupaten(kabupatenRequestVO);
            }
        };
        return handler.getResult();
    }

//    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "List Kabupaten")
    @RequestMapping(value = "/list-of-kabupaten",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfKabupaten(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                          @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                          @RequestParam(value = "provinsiId", required = true) String provinsiId,
                                                           @RequestParam(value = "namaKabupaten", required = false, defaultValue = "") String namaKabupaten) {
        Map<String, Object> pageMap = kabupatenService.listKabupaten(page,limit,provinsiId, namaKabupaten);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }
}

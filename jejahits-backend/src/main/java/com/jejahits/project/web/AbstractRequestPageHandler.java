package com.jejahits.project.web;

import com.jejahits.project.exception.JejahitsException;
import com.jejahits.project.util.RestUtil;
import com.jejahits.project.vo.ResultPageVO;
import org.springframework.http.ResponseEntity;


public abstract class AbstractRequestPageHandler {

    public ResponseEntity<ResultPageVO> getResult() {
        ResultPageVO result = new ResultPageVO();
        try {
            return processRequest();
        } catch (JejahitsException e) {
            result.setMessage(e.getCode().name());
            result.setResult(e.getMessage());
        }
        return RestUtil.getJsonResponse(result);
    }

    public abstract ResponseEntity<ResultPageVO> processRequest();
}

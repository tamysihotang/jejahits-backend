package com.jejahits.project.web;

import com.jejahits.project.service.ProvinsiService;
import com.jejahits.project.vo.RequestVO.ProvinsiRequestVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/provinsi")
public class ProvinsiController {
    @Autowired
    ProvinsiService provinsiService;

    @ApiOperation(value = "Create Provinsi")
    @RequestMapping(value = "/create-provinsi",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody ProvinsiRequestVO provinsiRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return provinsiService.createProvinsi(provinsiRequestVO);
            }
        };
        return handler.getResult();
    }

//    @ApiOperation(value = "List Provinsi")
//    @RequestMapping(value = "/get-list-provinsi",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<ResultVO> get() {
//        AbstractRequestHandler handler = new AbstractRequestHandler() {
//            @Override
//            public Object processRequest() {
//                return provinsiService.listProvinsi();
//            }
//        };
//        return handler.getResult();
//    }

    @ApiOperation(value = "List Provinsi")
    @RequestMapping(value = "/list-of-provinsi",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfprovinsi(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                           @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                           @RequestParam(value = "namaProvinsi", required = false, defaultValue = "") String namaProvinsi) {
        Map<String, Object> pageMap = provinsiService.listOfProvinsi(page,limit, namaProvinsi);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }
}

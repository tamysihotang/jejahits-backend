package com.jejahits.project.web;

import com.jejahits.project.enums.Direction;
import com.jejahits.project.enums.SortBy;
import com.jejahits.project.service.KategoryService;
import com.jejahits.project.service.ProductService;
import com.jejahits.project.vo.RequestVO.KategoryRequestVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/kategory")
public class KategoryController {

    @Autowired
    KategoryService kategoryService;

    @Autowired
    ProductService productService;

    @ApiOperation(value = "Create Kategory")
    @RequestMapping(value = "/create-kategory",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KategoryRequestVO kategoryRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kategoryService.createKategory(kategoryRequestVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "List Kategory")
    @RequestMapping(value = "/get-list-kategory",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> get(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                        @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                        @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kategoryService.listKategory(page, limit, name);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Get profile Vendor by Kategory")
    @RequestMapping(value = "/get-list-vendor/{kategoryId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> retrieveData(@PathVariable String kategoryId,
                                                     @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                     @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                     @RequestParam(value = "sortBy", required = false, defaultValue = "RATING") SortBy sortBy,
                                                     @RequestParam(value = "direction", required = false, defaultValue = "DESC") Direction direction,
                                                     @RequestParam(value = "provinsiId", required = false) String provinsiId,
                                                     @RequestParam(value = "kabupatenId", required = false) String kabupatenId,
                                                     @RequestParam(value = "kecamatanId", required = false) String kecamatanId,
                                                     @RequestParam(value = "rating", required = false, defaultValue = "0") Integer rating,
                                                     @RequestParam(value = "vendorName", required = false, defaultValue = "%") String vendorName) {
        Map<String, Object> pageMap = productService.listVendorPerKategory(page, limit, sortBy, direction,kategoryId, provinsiId, kabupatenId, kecamatanId,rating,vendorName);
        return AbstractRequestHandler.constructListResult(pageMap);
    }
}

package com.jejahits.project.web;

import com.jejahits.project.service.KecamatanService;
import com.jejahits.project.vo.RequestVO.KabupatenRequestVO;
import com.jejahits.project.vo.RequestVO.KecamatanRequestVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/kecamatan")
public class KecamatanController {
    @Autowired
    KecamatanService kecamatanService;

    @ApiOperation(value = "Create Kecamatan")
    @RequestMapping(value = "/create-kecamatan",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> create(@RequestBody KecamatanRequestVO kecamatanRequestVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return kecamatanService.createKabupaten(kecamatanRequestVO);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "List Kecamatan")
    @RequestMapping(value = "/list-of-kecamatan",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfKecamatan(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                           @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                           @RequestParam(value = "kabupatenId", required = true) String kabupatenId,
                                                           @RequestParam(value = "namaKecamatan", required = false, defaultValue = "") String namaKecamatan) {
        Map<String, Object> pageMap = kecamatanService.listKecamatan(page,limit,kabupatenId, namaKecamatan);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }
}

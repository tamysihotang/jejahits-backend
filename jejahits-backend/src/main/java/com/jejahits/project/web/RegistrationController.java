package com.jejahits.project.web;

import com.jejahits.project.service.RegistrationService;
import com.jejahits.project.vo.RegistrationRequestVO;
import com.jejahits.project.vo.RegistrationUserRequestVO;
import com.jejahits.project.vo.RequestVO.ChangePasswordReqVO;
import com.jejahits.project.vo.ResetPasswordVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;



@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/registration")
public class RegistrationController {

    public static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    RegistrationService registrationService;

//    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Registration Customer/Vendor")
    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> registrationUser(@RequestBody RegistrationUserRequestVO userRegistration,
                                                     @RequestParam(value = "type", required = true)  String typeUser) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return registrationService.registrationUser(userRegistration, typeUser);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Change Password")
    @RequestMapping(value = "/change-password",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> registrationUser(@RequestBody ChangePasswordReqVO changePasswordReqVO,
                                                     @RequestParam(value = "username", required = true)  String username) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return registrationService.changePassword(changePasswordReqVO, username);
            }
        };
        return handler.getResult();
    }

//    @ApiOperation(value = "email notification")
//    @RequestMapping(value = "/email-notification",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public ResponseEntity<ResultVO> registrationUser(
//                                                     @RequestParam(value = "username", required = true)  String username) {
//        AbstractRequestHandler handler = new AbstractRequestHandler() {
//            @Override
//            public Object processRequest() {
//                return registrationService.emailNotification(username);
//            }
//        };
//        return handler.getResult();
//    }


//    @RequestMapping(value = "/user/{email}/", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<ResultVO> checkUser(@PathVariable("email") String email) {
//        AbstractRequestHandler handler = new AbstractRequestHandler() {
//            @Override
//            public Object processRequest() {
//                return registrationService.checkUserEmail(email);
//            }
//        };
//        return handler.getResult();
//    }
//
//
//    @RequestMapping(value = "/user/reset-password", method = RequestMethod.POST)
//    @ResponseBody
//    public ResponseEntity<ResultVO> resetPasswordUser(@RequestBody final ResetPasswordVO resetPassword) {
//        AbstractRequestHandler handler = new AbstractRequestHandler() {
//            @Override
//            public Object processRequest() {
//                return registrationService.resetPasswordUser(resetPassword);
//            }
//        };
//        return handler.getResult();
//    }

}

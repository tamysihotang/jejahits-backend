package com.jejahits.project.web;

import com.jejahits.project.service.CustomerService;
import com.jejahits.project.vo.RequestVO.CustomerRequestVO;
import com.jejahits.project.vo.ResponseVO.CustomerVO;
import com.jejahits.project.vo.ResponseVO.VendorVO;
import com.jejahits.project.vo.ResultPageVO;
import com.jejahits.project.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;

//    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Search Customer")
    @RequestMapping(value = "/list-of-customer",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultPageVO> getListOfCustomer(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                           @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                                           @RequestParam(value = "customerName", required = false, defaultValue = "") String customerName) {
        Map<String, Object> pageMap = customerService.listCustomer(page,limit,customerName);
        return constructListResult(pageMap);
    }

    protected ResponseEntity<ResultPageVO> constructListResult(Map<String, Object> pageMap) {
        return AbstractRequestHandler.constructListResult(pageMap);
    }

    @ApiOperation(value = "Get profile Customer")
    @RequestMapping(value = "/get-detail-customer/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> retrieveData(@PathVariable String id) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return customerService.detailCustomer(id);
            }
        };
        return handler.getResult();
    }

    @ApiOperation(value = "Update Profile Customer")
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultVO> updateProfile(@RequestBody CustomerRequestVO customerVO) {
        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return customerService.updateCustomer(customerVO);
            }
        };
        return handler.getResult();
    }
}

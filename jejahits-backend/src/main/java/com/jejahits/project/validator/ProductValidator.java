package com.jejahits.project.validator;

import com.jejahits.project.vo.RequestVO.ProductRequestVO;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ProductValidator {

    public String checkMultipartFile(MultipartFile multipartFile) {
        String fileName = multipartFile.getOriginalFilename();
        String ext = FilenameUtils.getExtension(fileName);

        if (!ext.equalsIgnoreCase("jpg") && !ext.equalsIgnoreCase("jpeg")) {
            return "File format harus JPG/JPEG";
        }

        return null;
    }
    public String validationCreateProduct (ProductRequestVO productRequestVO){
        if(productRequestVO.getHargaRangeAwal().intValue() > productRequestVO.getHargaRangeAkhir().intValue()){
            return "Harga awal tidak boleh lebih besar dari harga akhir";
        }
        return null;
    }
}

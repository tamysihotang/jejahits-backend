<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Backend API</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="style.css"/>"/>


</head>
<body>

<h1>Backend API</h1>

<div id="content">
    <h2>Home</h2>

    <authz:authorize ifNotGranted="ROLE_ADMIN">
        <h2>Not Authorized</h2>
        <a href="login.jsp" >Login Page</a>
    </authz:authorize>

    <authz:authorize ifAllGranted="ROLE_ADMIN" >

        <%--<ul>--%>

            <%--<li>--%>
                <%--<a href="${pageContext.request.contextPath}/swagger-ui/index.html">Insantani REST API Documentation</a>--%>
            <%--</li>--%>
            <!--<li>
                <a href="https://drive.google.com/file/d/0B-pin_01MQ_-VUo3WGZzNFlPOXc/edit?usp=sharing" >Android Client App</a>
            </li>-->
        <%--</ul>--%>

        <div style="text-align: center"><form action="<c:url value="/logout.do"/>"><input type="submit" value="Logout" /></form></div>

    </authz:authorize>
</div>


</body>
</html>

#!/bin/bash
#  scp -i id_rsa jejahits-backend/icon/shirt.png nostra@35.185.190.242:
#  ssh -i id_rsa nostra@35.185.190.242 "sudo mv shirt.png /opt/project/upload/icon"
scp -i id_rsa jejahits-backend/target/jejahits_backend-1.0.0.RELEASE.jar nostra@35.185.190.242:
ssh -i id_rsa nostra@35.185.190.242 "cd /opt/project && sudo ./projectctl stop"
ssh -i id_rsa nostra@35.185.190.242 "cd /opt/project && sudo rm -rf jejahits_backend-1.0.0.RELEASE.jar"
ssh -i id_rsa nostra@35.185.190.242 "sudo mv jejahits_backend-1.0.0.RELEASE.jar /opt/project"
ssh -i id_rsa nostra@35.185.190.242 "cd /opt/project && sudo ./projectctl start"
